---
title: "Workplace by Facebook: Wider Post Trick"
date: 2019-06-29T16:10:20+08:00
feature: "images/post/workplace/blur_wp_wide.png"
summary: "A small css trick to get a wider Post in Workplace by Facebook. Less scroll, more read!"
draft: false
---

Recently, the team changed the main communication tool. This time is quite radical because we are stopping our most active and loved Slack, and start a full-focus trial run with [Workplace by Facebook](http://facebook.com/workplace/). Reason: We want less synchronous communication (chatting) and want more asynchronous one (well thought message with well thought comments). Basically: [Basecamp said it](https://m.signalvnoise.com/is-group-chat-making-you-sweat/).

We liked it so far. The environment encourages us to type longer message (which naturally put more thought into it before publishing). And we don't expect the other party will reply immediately, like one mistakenly expects in a chat room.

So, yeah, we utilized the "Post" feature a lot. It is exactly like what normal Facebook post behaved. We start a topic with a well formatted message, then others start answering question, asking more question, exchanging idea. All in the context of that Post. A nice way to keep different track of topics on going.

## Here's come the problem...

![alt-text](/images/post/workplace/blur_wp_short.png "Too narrow")

The Post, for some reason, is confined in width. In one fine day after we exchanged a few comments in the same post. Me and the UX designer realized we have been staring at the small boxed area all these time. Reading long comments in that box, and type our own long comment somewhere below the same box. All take up 50% of total screen width, and snapped to the center. Needless to say, forced to scroll more often than necessary.

The UX designer decides to send a feedback. I hope she got it through because after several tries, I can't send mine. Claiming there is an error. So I gave up. Sending the feedback, but I didn't give up solving this problem!

## A little css trick

After fiddle around with the cryptical maze of Facebook's css, I pin-point the single id selector which does the trick. With [Stylish](https://userstyles.org/) in Firefox and put in my own hack for `https://my.workplace.com`:

```'css'
#contentArea {
    width:80vw !important;
}
```

![alt-text](/images/post/workplace/blur_wp_wide.png "Must wider")

I used `80vw` because I thought that gave me the best view. If you are doing the trick, just play with that value. 80 ~ 100% is not bad.

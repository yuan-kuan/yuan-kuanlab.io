---
title: "What I learned after teaching few kids programming for half year"
date: 2019-12-30T19:00:00+08:00
draft: false
feature: "images/post/lesson-learned/intothebox.jpg"
summary: Creativeness is not learned, it was locked away
---

_disclaimers_

Time and number of students matter: It is a weekly 2 hours lesson on Saturday afternoon, which involve around 8 kids. Then a single 5 consecutive days of game design & animation workshop for 19 kids. My experiences are drawn from teaching these kids and these hours only.

Other contexts that matter: This is Kota Kinabalu, Sabah, Malaysia. Not a modern city, but we do not lack of well-off middle class young families with educated parents, money to buy smart phones and broadband. Which is mostly the family background of my students.

These are, at best, my own experiences that I would like to share. Enjoy.

![alt-text](/images/post/lesson-learned/intothebox.jpg "Into the Box")
_**Into the Box** commisioned art from [elsearts](https://www.instagram.com/elsearts/)_

## Creativeness is not learned, it was locked away

Saying how one can "teach" kids to be creative is nonsense. They had it. Lift enough mental barricades and adults-imposed boundaries, their creativeness ooze out of them.

My students usually do not use the same character I used in my lesson. They chose their own from the library, or paint their own. They rarely finish my planned lesson without asking how to do something else in the middle, e.g. different kinds of enemy movement, press a key instead of clicking a button, say more than just one line of dialogue.

One doesn't learn how to be a creative person, they just slowly becoming a less creative person over time. They lost it to school's examination grade. They blocked it for red crosses in their work book. They hid it from adult who said "don't waste time in this". They gave it to "real life". We gave it up to real life.

I try not to restrict their imagination, but not all fantasy is achievable, at least not in 2 hours of lessons. I do, instead, made it clear that if they want to do what they want, they can. Just master the craft. Imagination knows no bounds, but, no short-cuts either.

## If they like it, they like it

Vice versa.

I'd heard people say some kids just do not know what they like, but one can foster their interest by grinding them through the lesson/class/workshop, whatever. Not sure how true that is, but I still hate school taught English or History even after grinding through them for 12 years.

Majority of my students show genuine interest in making games with [Scratch](http://scratch.mit.edu/), or solving fun puzzles with basic programming language in [CodeCombat](https://codecombat.com). You can see the sparkle in their eyes as soon as they realize the skills they are learning will allow them to create a whole new world. Their creator's drive kicked into overdrive. They will start tinkering with their games on their own, break stuff to express themselves in this brand new world.

But not everyone. I had students that sat there idling, waiting for instruction, has zero will to solve the next problem by themselves. They don't even ask questions or voice out their own imagination how the games/world/story should be. My coding class is like another Mathematics class for them. There is nothing wrong with that. It simply means they are not into this, they should spend time on other stuff they like.

People get use of stuff. We adapt. Given enough time and attention, we all master some sort of skill that we grind through. Here's the question though, why we grind the kids through something they initially don't like? Is programming really that important? Should piano skills take priority over reading a story book? Why Karate? Why Ballet? Why not they just spend the hours doing something they genuinely love, like painting or building castle in MineCraft?

## Parent is the sole factor of how well a kid will learn

This is the most sensitive topic I will talk about in this blog. Parenting is hard and personal, everyone has their own way of doing it. Ultimately, I am not even a parent myself. I am merely talk about my own observation with my super small data: my students.

There are some parents I never met personally. Some I saw them waiting at the door while picking up their kids from the center. They pay a tuition fee, drop their kids in and pick them up later. Few of them picked up by driver or maid.

There are parents who walked up to me and checked out their kids' interest and progress. Asked my opinion of how they can do better to let their kids learn.

If parents thought knowledge and wisdom is purchased with money and time. Their kids think so too.

If parent thought learning involve the kids and parent's attention, their kids think so too.

## Mobile devices stole kids' creation time

Personal Computer is a luxury that I enjoyed during my childhood. I owned a 486 when I was 10 years old. Being in a small town in Malaysia during 1995, I have no idea how lucky I was. PC then becoming mainstream. Kids and teenagers start owning their own PC at home. I thought that would be the way for a long time.

I am wrong.

Fast forward to present time, 2020, I am actually surprised that, most kids, even those from middle class family, being sent to tuition center with a noticeable fee, have 0 exposure to computer. What they're good at? Mobile devices.

Mobile devices and PCs are not the same thing. Mobile devices is consumption device. They are best for consuming information. Watch video, read article, play games, clicked through social media. Computer is primarily a creation device. They still excel in consuming information, just less cozy as you can't use it anywhere. But the real deal is, PC lets you create.

There are only so little thing one can create with an expensive iPhone 11 Max Pro. There are infinity stuff one can create with a cheap laptop.

## End's notes

I believed what I learned is valuable especially to young families with young kids, thus I share it out even though I think these are all personal feeling formed by my personal observation. I used "I" a lot but actually all these thoughts are shared and discussed with my wife (ElseArts).

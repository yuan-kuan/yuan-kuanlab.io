---
title: "AngularDart + CouchDB = Fullstack"
date: 2019-05-01T23:24:00+08:00
feature: "images/ADC.png"
summary: "Without a single line of backend code. Not so much about AngularDart. You can swap in with any frontend technology to achieve this. In other word, if you are AngularDart programmer, this may be a good alternative for your backend solution."
draft: false
---

![alt text](/images/ADC.png "AngularDart CouchDB")

Without a single line of backend code. First, lets drill down the techs. Second, I will tell a bit of story why I chose to do it this way.

## The real heroes are CouchDB and PouchDB

Not so much about AngularDart. You can swap in with any frontend technology to achieve this. In other word, if you are AngularDart programmer, this may be a good alternative for your backend solution.

### Seriously, why everyone not using [CouchDB](https://docs.couchdb.org/en/stable/intro/why.html#intro-why) yet?

I am not even referring to its most powerful feature -- the sync. I am talking only about its REST API. The feature that allows me to skip the whole middle layer backend for my database CRUD operation, for map/reduce, for simple Mongo Query. Host CouchDB onto any web server, exposing its REST API, and I get everything.

### Even better, I skipped managing HTTP calls by using [PouchDB](https://pouchdb.com/)

PouchDB did two important thing for me.

1. It encapsulate the HTTP calls with the CouchDB and allows the front-end code to use its classes and interfaces, without handling the HTTP calls.
2. It can sync with the remote CouchDB, and take the DB offline in the browser. And sync back later, harnessing the most powerful feature of CouchDB.

I ran my pouchDB in multiple worker thread, each handling different categories of data. These communication with the PouchDB/Web Workers are isolated from the UI/view part of the code, and used as a service. A service that make remote database operations inside the browser, without the need of a middle layer.

If you are building a [PWA](https://developers.google.com/web/progressive-web-apps/), PouchDB is one of the best choice, if not the best.

---

Well that's the *backend*! Now we will talk about the *frontend*.

## [AngularDart](https://webdev.dartlang.org/) as the front-end tech choice

1. I afraid of JavaScript.
2. I was halfway through [Flutter](https://flutter.dev/) and fall in love with Dart.
3. Google used AngularDart for their big web application, like AdWord.

I never make a real application with JavaScript before so I not sure how much I gained or lost by sticking to AngularDart. I do know I have a live web application running with it without too much issue (beside all my bugs) and most of my hair still intact not being pulled or scratched away. I think I am doing okay.

### Angular philosophy is suitable

The idea of services in Angular framework works well with doing database operation in the browser. Component use the service without knowing where the process took place, it can be in another worker, in the server, or it is actually a serverless function. Component just wait for the result.

So, instead of data service that call an API via HTTP and wait for the result from the internet, we do the CRUD with the database directly in the browser. Component doesn't know, it just get the result and display it.

### JS Interop to bridge the gap

Though Dart is fun to write, it lacks strong third party libraries for web application. This gap is sort of closed with the [JS interop package](https://pub.dartlang.org/packages/js), which allow AngularDart program to access JS libraries. Without this, I don't think it make sense to use AngularDart for serious web application, unless you have a big team of programmers to reinvent a lot of wheels.

Most importantly, I relied heavily on PouchDB -- A JS library provides functionality of CouchDB in the browser. The ability to use this battle tested library off the shelve in my application is crucial to the success.

### [Material](https://material.io/) designed component

Not exclusive for AngularDart but it almost felt like a native UI to use alongside AngularDart. It is beautiful and tidy web UI without doing anything extra.

---

## That's all you need for a working web application

You have the frontend that make its own database CRUD, view operations. Just host the CouchDB in the internet, and launch the frontend like any other [SPA](https://en.wikipedia.org/wiki/Single-page_application). You're done! Too good to be true? Depends.

I had my web application live in the internet, but it was a free-for-all, and that's bad. I almost thought my plan is foiled when I started to think about the most fundamental function of any web application -- user management. I didn't know any other way to do it without my own server. But there are indeed ways...

### User Authentication and Management with [Auth0](https://auth0.com/)

The web development techs and landscape has changed so much when I was busy typing code for games. It seemed that writing your own user management system is not only tedious. It is also outdated, dangerous and reinventing a bad wheel.

With half day of learning and setup with Auth0. I got the most secured user management I can dreamed of. Everything is covered, actually like 10 times more functional and secured than anything I can put up in a month. And it is free! (to some extend of course, everyone need to pay for their lights and food).

### Serverless functions to pick up the slack

Worst come to worst, you can also spin up a serverless function, like Google's Cloud Function, to handle any isolated function. Top of my mind is periodic long process like cleaning up the database, backup, perform heavy process during down time, migration.

I did forced to use one serverless function to make signed URL for access to Google Cloud Storage, because I am handling the file storage on behalf of the user. But other than that, I haven't meet another problem that warrant more of it.

---

Here's the second part, where is more about personal feeling instead of rational reasons. Though, they can be the same thing.

## I do not hate server programming

In fact, I am still interested to learn more about Go and use it for backend. I just happen to be in a situation that I need to cut down technologies and new skills I that need to learn, maintain, and teach the next person.

Sounds all too familiar for solo developer who tried to make a startup works? Indeed I am just that, no code partner to share my work and shout at my stupid idea. (If you somewhat intrigued by this project architecture or just bore and like to work on a project, do ping me, I can bring you for a walk in my project!)

Of many reason I came across CouchDB during my database research, the support of REST API (more like, natively the only way to communicate with CouchDB) is the biggest selling point. I failed to seduce my server programmer buddy to join me around the same time, so this "skipping the whole middle layer" become a life saver.

I not sure how long can this withstand the test of wild real world use case, but I can't afford to change until I got a server programmer to take up the job.

## Why AngularDart? Another JS hater?

I just afraid of JavaScript. Tried a few time with my pet projects when I still had my full-time job, I just can't get it. Bite by it's hard to reason runtime weirdness couple of time and I developed my anxiety with dynamic Language. TypeScript pulled me back for a while but not for long...

Dart 1.0 was a pretty dynamic language but in Dart 2.0, they gained sound type. Which is pretty good. I was in the midst of learning Flutter (which use Dart) when my startup partner find me for my current web application project. I did a quick Google search and found out I can just continue using Dart for web frontend. So far I am liking it, just not sure in what form this will bite back. Maybe it is already biting back...

Finding another AngularDart developer to join the team can be a tricky task though. I've never meet a friend that I don't need to explain what is Dart, and Flutter. Of course the fact that I have a decade in games development does not help with the odd my old friends know about trendy web languages.

## Last word of the first post in my own blog

I am glad I didn't do a "Finally another Hello World blog for me after couple of other failed blog".

If you stumbled upon this and like to talk about this or anything else, you can reach me in all the social media link below. Still not sure if I want a comment section here.
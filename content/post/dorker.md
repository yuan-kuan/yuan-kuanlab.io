---
title: "Dorker: Dart's Web Worker"
date: 2019-06-16T12:17:54+08:00
draft: true
---

_[Part 1]() detailed what I found out about the Web Worker situation with Dart's web and my struggle to find a solution._
_[Part 2]() detailed my solution and the internal working of it._

And this is Part 3, which is introduce a friendlier [Dorker 0.2.0]() and a sharing of how'd I use Dorker in my current project.

## Easing the communication with Web Worker

Dorker made communication between the main application and Web Worker possible (or between Web Workers), but it does so with passing plain, simple buffer as message. That is error prone, and I did ran into weird bug with poor error reporting, just because I kept forgetting to encode my object to String, or decode the String to an object. All these problems while I was still the only programmer. I knew I need a more disciplined way to use Dorker.

### Request and Respond



## Angular Dart Web App powered by Web Workers

Months passed and I buried my head deep down into development of my startup's project. It is not ready yet, but ever since I put it online and live on the internet, getting abused by testers and myself, that version is always running at least 4, sometimes 6, Web Workers via Dorker. Can't really say it is battle tested yet, but it is something and I am confident to continue using it.



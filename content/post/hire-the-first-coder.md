---
title: "Hire the First Coder"
date: 2019-05-10T21:08:57+08:00
feature: "images/post/hire/faces2.jpg"
summary: "To seek another brain + pair of hand to help me shaping up the web app. Also to be more prepared for the imminent big push if my partner suddenly land a deal. If you read the post, you can sort of guess I have some hills to climb. I will talk about them here anyway."
draft: false
---

![alt-text](/images/post/hire/faces2.jpg "Faces")
_**Faces** commisioned art from [elsearts](https://www.instagram.com/elsearts/)_

5 months have pass since I started working on my current startup. I still fail to seduce any of my close coder friend to join me.

Beside being really lonely to watch the list of git commits with only my name on it, the project really picking up speed in freshing out design, piling up sketches and documents but most of these are *not* in codes.

That's the problem when you found a very good project manager, who turns in to a good UX researcher in a week, to join your project. Let her runs the show for a month, suddenly programming team seems like the one dragging the feet of the team. Of course, that is a good problem to have.

---

## So I Wrote My First [[Hiring Post](https://flutterjobs.info/job/ff5wblPzrQzwr3I355A9/)]

To seek another brain + pair of hand to help me shaping up the web app. Also to be more prepared for the imminent big push if my partner suddenly land a deal. If you read the post, (_It will be taken down after 22nd May, don't blame the free service as I can't pay 30 dollar to post 30 days!_), you can sort of guess I have some hills to climb. I will talk about them here anyway.

### We Are Poor, Can You Work For Free?

Yes, I actually asking people to work for free in a job post! With the ever sweet profit sharing promise, really sounds like a scam, even we all know it's not, are we? Friends and family probably trust me with that, but for stranger, I have no idea.

Me and my partner decide in the earliest time that we are not going down the "raise fund, spend big, and then what?" route. We do it the most traditional way. Make some sales, minus the cost, profit. Well, that make us poor at the beginning. If I failed to sell my dream token, I got no one working with me. Here we are.

### We Are Still No One

Since the website is still brewing, I didn't even link it up in the job post. We are getting it ready for first client meeting. Regardless, we are still no one. People hasn't heard about us at all. How good is the team? Can we be trust? Where are we? Are we even human?

The bright side: When a team hasn't make up an identity, being part of it in the early day will help forming up that identity. Injecting a lot of you into the team. Not I am with that team, but I am part of that team. Can this be one of the selling point for unknown startup like us? I certainly hope so.

### Asset Management Software, Boring

Not a Pokemon battle royal games. Not a Facebook killer. Not an Apps that change the face of the earth. Not a watch that know exactly when you should pee again. Not harvesting the moon.

But, it may be the one that help companies with a lot of machines to stop killing their workers by malfunctioning equipment. Not too boring now?

### Virtually Zero Requirement. Beggar Can't Be Chooser?

I didn't put any skill requirement (beside knowing English and programming, but... why are you there if you didn't know these 2?). I didn't put experience requirement, and let alone school and what not. It feels like because I am asking people to work for free (with profit sharing!), I can't be chooser.

It is not.

It is actually a set of filter. I filter out people who thought I am not serious because I don't care about their career achievement, I don't care how hard they earned their degree/master/phd, I don't care how deep or wide are their skills level. Don't even get me started with requirement like "great team work, self motivator, fast learner, manager of own time". Seriously? Typing that out in resume will make a person into that kind of personality? Wow.

I am serious though. But I really don't care about these. People can say anything about themselves, I just don't buy it. If they think these are what make them successful or worthy of this job, I will pass. They'll find their way somewhere else. I don't know what kind of person I will attract. At least I filter out those I don't share the same opinion about what qualify as a coding partner.

I am looking for a person that just enjoy programming and solving problem with it, and withstand my naiveness. Simple as that. (well, of course, this person has to be good at it to some level, else I am giving myself more trouble)

---

## Respondents

Oh yes. Before anyone has more time to think Kuan is crazy and this job posting is a failure. I've got 7 responds in less than 48 hours. Given an estimated 2k impressions for 14 days, I'd say this are pretty impressive responses. Pretty fun as well. I've got people from all around the world, Iran, Australia, UK, Eastern Europe, USA, Italy. With me at Malaysia, partner at Australia, UX designer at USA, I still have a good shot at forming a multi-continent team.

### The Resume/CV Dropper

Of course there will be obvious "Open the post, skimp through the text, click that link, drop the resume/email". One even without a subject! Not that I discriminate resume or something. I did mentioned at least send me some code if one does not have online repository. These I didn't even reply.

### The Outsourcing Team

Just got one that coming in and say I can end my search because this paid development service found my post. Not sure which part of "I can't really pay" in my post isn't clear. Seriously, if your team already have all those 60 multinational clients project worked on, give other emerging programmer some chance. And obviously I can't pay for your "fixed rate".

### The Student

Actually I admire this guy who ask to join my team while he's still in his year 2 University. I think he is already ahead of many other peers, by just actively reaching out to real world project before he even finish his study. I replied him to focus on his study first, partly because I have no idea how good he is to juggle a job and the study, another part is, I need a full time commitment if I can find one.

### The Sincere Human

The rest are the kind of person I am looking forward to. They said Hello, they crafted their email, like a human would before you talk to another human. I tried my best to reply and find out if we can fit each other. (meaning, they fit into ANA, and ANA fit into their goal.) Few not, few still on-going. Good stuff.

At least I am not too crazy after all.

---

## How About You?

If you made it this far, and happen to know programming, would you consider to join ANA and help pushing it to the success? Talk to me!

Thank you for reading. I hope I didn't hurt anyone feeling, especially the part about job requirement. Everyone has their perspective for life. I am sorry if my word is too harsh. Cheers.
---
title: "My Games Dev Journey: What's so Fun?"
date: 2019-07-07T16:20:46+08:00
draft: true
---

This is personal. My own story of leaving dream job(s). Hope this turns out to be a fun read for you.

## A Decade of Games Development

If you are not from Games industry and thought games developer job must be fun, well, it is fun! I don't think I ever hate my job for the past 10 years. Beside not able to join Blizzard Entertainment, I was living the dream.

This "fun" though, might not as you expected, or how I initially imagined before I start waving my name card as a game programmer.

Get to play and test games in a job sounds fun, but not after you play and test the same damn thing for months, everyday. Not to mention these game testing sessions spat worms out of it and I have to squash those bugs.

Surrounded by games console/figuring/poster or fancy office decoration made a fun "Games Studio Tour" video, but not fun anymore after 2 days. You ignore them and just walk from the door to your desk and look at the same white/black codes in your screen.

Nerf gun war? That's just stupid. It is awkward.

## The Joy is Solving Problems

My first job requires me to tap in & out with those old school time card. In 2008: A game studio is requiring employee to pick up their time card, put into a machine, then it made a printing sound, take it out, slot it back. "Okay..." _disappointment toned_. Bank used to have these when I visited my mum's office as a kid (1990 ~ 1996). The boss never really friendly to begin with, and he demands respect and loyalty out of the thin air. The whole culture is basically: You owe the company for giving you this job, now work.

Despite such rigid working environment, I still had a blast in the short 1.5 years tenure, shipped 2 games as a junior game programmer. In this job, I got to explore so many different aspect of games development and enjoy a great degree of freedom to navigate them. Tons of fun. And they are mostly stem from the tons of interesting problems waiting, or flying towards, me in the day to day work. Touch based UI before it is such a norm, branching story line, codec save file, Wii-motion+ sword fighting mechanism (my favorite), Wii "hi-end" graphics. Lastly, a rather bizarre Wii 3D graphic "wrapper" library.

These solved (or unsolved) problems all stuck in my head, alongside the friends who I made during that period. I do hope these are not some maniac workaholic mental problem that one remembered _problem_ from their previous jobs, but, let it be meaningful and rewarding memory that the brain decided to store. Akin to how one does not remember a single essay they forced to wrote during school exam, but remember well the kite one built as a kid?

In games dev, it is rare that a developer is lack of problem to solve. Games makes these problems slightly more interesting, and more challenging as well. **Thus, solving games' problems is fun!**

## Team Games always Fun

Needless to say, I didn't last long in such rigid company culture. End up quitting the job before looking for another in this foreign country. As lucky as it could be, I joined Ubisoft Singapore, a AAA game studio! Hired straight into the UI team, and with the same team until I leave 4 years later.

The problem domain is narrowed, but deeper. The scope of the games is much larger here, and thus the complexity of the problem multiplied as well.

Now, more seniors will review the code, and deal with my naiveness and sloppiness. At the peak of the development cycle, I have couple of other programmers working together in this narrow scope of UI. Partnering up with 2, later 3, at peak 4 UI designers, while there are games designers who decide how UI should behave and fit into the games. A rather big development team just for UI, all these does not include the services (backend) developers, that team is like 5 man strong.

Almost every pixel that appear on the screen is a joint effort of a few humans. No longer there is solo stupid solution that made it to the end product, like my codec save file in my first job. Whether it is self-invent, larger-than-life, ground-breaking UI fanciness, or really out-of-scope multiple-man-years-effort UI toughness. They are all team effort, in a good way or in a bad way. For me is all good ways (not because they will read this blog, but seriously, we rocks!) **Solving a problem is fun, getting it done as a team is fun fun**. But scrum meeting and daily standup, again, stupid. Stop that.

## Games' Problem

In games development, problems are usually self defined. The real world problem of a game is usually just one -- provide a fun experience to the player. Ok, too old-school, the evil one -- provide an addicting environment which seduce player unconsciously pay more than one's really should for a games. This problem is usually tackle by the games designer or UX designer: how to make a games fun (or addicting). In the process, they spawn games design, idea, mechanic, art, tech requirement, etc. Then, only by then, the rest of the team have their problems defined and start the battle.

The designer think the player will have fun shooting a bird to a block of woods, in iPhone. John will draw a bird, Lee will make the physics of those blocks, Kitesh make the bird fly after a flick of finger. Did the customer (the player) wants this bird game before? No. The designer wanted this bird game. This problem(games design) are spawned and defined within the team.

Usually, the games dev team is a close tight group, they are single unit to produce a games together. Thus, the problems are bendable to some extends. Kojima can have the wildest open world mechanic that almost all NPC behave like those on earth now, but since he is within the same constrain with the whole dev team, he can't just throw the problem to the artists and programmers and say "do it". He need to shape his design (thus the problems for the rest) until it fit the scope of the team (time, skill, money, CPU power). That's what I meant by "self defined" problem.

## Real World Problem

My lucky strike didn't stop and I somehow squeezed my way into Unity, the games engine startup (then. Now is a tech giant). Eye opening experience is almost an understatement for the next 5 years. Lets start with the problems (Problem, problems, problem! So much problems!)

Unbeknownst to the me who signing the Unity job offer, games engine development, i.e. tools making, is different kind of problem solving. The problem is real, out there, customer/user is trying to solve a problem on their side, using the tool.

Not exactly is "user problems are our problems". But more like "how user solve their problems, are our problems". Their problem is making a bird fly across a phone screen after a flick of finger. Our problems are: How to detect the finger movement, how to let user define each pixel of the bird, how to show this pixel on a phone screen, will user want gravity, but how much, or there is a better way for them to engineer their own gravity, so on and on and on.

The craft doesn't end at merely letting user achieving their goal (solving their problem). It extends into how easy did the user solved it with the tool. Did the user enjoyed it? What kind of pre-requistes knowledge the user need to use the tool? Did the tool improved their life? Roughly known as the UX, user experience.

## True Empowerment

Not too long after my entry, I was appointed to take over an off-shot version of Unity. The feature is called cluster rendering, a special setup that sync up the display from different simultanuously running computer, and mesh those display in a big all, or box them up. My responsible is maintain it, add new functionality, run the beta testing and stuff. Basically I have to own it. Bug? fix it. User asked a question? Answer it. New functionality? Type it. Has new version? Launch it and announce it.

Because of this unprecedented empowerment, I finally tasted the bitter sweet reward, albeit little: The Impact.

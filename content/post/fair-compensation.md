---
title: "Our Compensation Plan"
date: 2019-06-03T10:00:45+08:00
feature: "images/post/fair_compensate/scale.jpg"
summary: "This is the plan about how me, one of the boss, is going to treat the team fairly, and make everyone work until they retire or decide quit working and save the planet, but not for Google, Facebook nor 7-eleven."
draft: false
---

![alt-text](/images/post/fair_compensate/scale.jpg "Balance")
_**Balance** commissioned art from [elsearts](https://www.instagram.com/elsearts/)_

_We do not have the money to pay for monthly compensation now, will you work unpaid with a profit sharing plan?_

The above phrase is in my recruitment post, in my first email replied to applicants, and in my compensation plan too. Crazy it may seems, but I do have interested person who continued the conversation. A few took up the challenge to trial working with me, too. I am super happy with this outcome.

What is the *PLAN*, though?

Before I paste the plan, here's some background: I have been a fans of Jason and DHH -- co-founders of [Basecamp](https://basecamp.com/), and avid reader of all their books. Even during my time as an employee, I have decided that if I ever run my own team, I will just do what they do. After writing a dead simple and honest job posting with no emphasis on formal education, writing up the compensation plan is next, sharing it out open in my blog is next's next.

Behold! If anyone ever join my team (and we become successful), this is what one will get.

---

## The Plan

_This is the plan about how the company is going to treat the team fairly, and make everyone work until they retire or decide quit working and save the planet, but not for Google, Facebook nor 7-eleven._
_This is only for the earliest days, like now. This plan should adjust for betterment of everyone as time move on and the company is growing and on track._

**Current Situation**: No income, no capital, no cash, no liability. Everyone is working for free.

## Monthly Compensation

At the moment, we can't afford it. But I'll outline the plan anyway.

Once the company start generating income, we will start paying monthly salary to the team. Everyone need money to pay bills and buy food. There are 2 factors that determine how much is the monthly payout: Role and time spent.

### The role

All salary will be the same for everyone of the same role, regardless of city one living at, i.e. everyone will get the same pay that match their role in the market value. At the beginning, we will follow Singapore/Australia average pay for the role, and pay the low 10%. e.g. For a senior designer, check the average pay for senior designer, look at the lowest 10% pay they get, we pay that.

Kuan copied this idea from [Basecamp's handbook](https://github.com/basecamp/handbook/blob/master/making-a-career.md#pay--promotions). After a decade of employment, salary secrecy bothered me the most. I do not want anyone think we are min-maxing our cost by negotiate different pay for everyone, and the bad negotiator get the worst end of the deal. Same work, same pay, no guessing. And no star player.

The lowest starting pay is to keep overhead low, spend the money wisely, and build up the muscle to sustain longer. If we runs out of business, everyone lost the jobs anyway. We shall raise this standard salary pay higher as we become more stable and generating more income. The pinnacle should the the top 10% of Silicon Valley pay (or whatever is the highest at that moment). If the team pushed the company to that kind of success, they deserve that kind of pay.

#### What/who determine the role

The leads or managers will, but we don't have any. So, for R&D, Kuan will determine. And he will just use this [heuristic](https://github.com/basecamp/handbook/blob/master/titles-for-programmers.md) for the time being.

### Time Spent per month

Everyone will say how much time they can contribute into the project for a month before starting of the contract. The number of hours per week will determine they are full timer or part timer.

The hours are provided by the team members themselves rather than a justification by anyone else. Especially for remote work, no one know how much one really work beside herself/himself. Not that we equal total working hour to effort anyway. But these must be high quality hours they able to commit. Not 1 hour after a long day and before sleep.

#### Full Time (30 ~ 40 hours per week)

As long as the crew think one can commit more than 30 quality hours per week to work, they will be pay for a full-time salary.

Beside a full-focus working hours of 6/7/8 per day. This include less hectic or less demanding hours for the rest of the crew's day. Meaning, if the crew has other demanding work commitment or long hours part-time job beside the project, this is a no-go for full-timer. We will ask said teammate to slow down on our project and just commit part timer hours.

Software development is a focus and creativity demanding work, these simply not available if the developer already spent their energy elsewhere, or depriving sleep for pushing more hour into work rather than rest and sleep.

#### part time (15 ~ 20 hours per week)

Inevitably, we will have person that cannot commit full time hour into the job, yet we need any contribution we can get. So we going to allow part time for a while. Of course, we hope eventually we generate enough income to switch everyone to fully focus at the project, as the only job.

While full timer get the full monthly salary compensation. Part timer will get 50% of it.

## Profit sharing

Salary reflects one's market value for their role. Profit sharing reflects one's contribution to the success of the project.

How are we going to do profit sharing in the middle/latter stage is out of mind now. But we need to worry about how to share the first few round because that is our promise to the pioneers.

At the early days, which is NOW, everyone is holding on the dream and potential of the company. Teammates are spending their time and effort helping us to push the project out to the market, while getting nothing or lowest paid (after we can pay) in compensation. They got only a promise for a share of the profit when the company turns some.

### Time Based profit sharing

We will simply use the time a person has been with the team as the share. Calculate how many days one has spent with the team. Total up all days of all crews, divide the money.

To be fair for full-time committer vs half-time committer. Each month, each team mate will report how many commitment they have made for the project in that month. This is just a rough estimate, and will either add 30 days for full-timer (30/40 hours per week) or 20 days for anything less than that.

It just an honest reporting. I hope everyone we get into the team is honest and fair to other team mate. Thus, if you didn't pull the full weight that month, give the full timer some credits.

### The share portion and capped

The company generate income by selling solution. Deduct the total cost from this income, leave some profit. Bosses then decide how much this profit should roll back into the budget and growth of the company. If there's anything left, we share it.

There is no owner cut in first few round. Instead, owners are simply the one spent the longest time in the project and naturally have the biggest share in the first few rounds.

But the payout will be cap at maximum 10k AUD per month. e.g. Kuan had accumulated 20 months until the first profit sharing. The maximum he can get is 200k AUD. This is solely to prevent we did a huge payout and destabilized the finance for upcoming years.

### How many is "first few round"

Right after the first round, we will start adjusting the profit sharing scheme. We will retain a portion of profit share for the pioneer group but this privilege should slowly chip away to make space for performance base sharing. This is to ensure we do not discriminate late comer too much, and all contribution at the present moment is the most critical.

If pioneers are still with the team, they should already in a more important role and seniority to have a decent monthly reward. With pioneer share + performance share, they still benefit from the earlier day's hard work + continuos contribution.

### new comer

New comer will need to first pass the probation period (1/2 months) before they are eligible to be part of the share, the calculation of the time for profit sharing is started from the day they join, not when they pass the probation. If said developer failed the probation, we will have to ask them to leave the project.

Probation is to find out the attitude, commitment level and skill level. If a super star pulled 80 hours week (not allowed, by the way) and chopped down features like Chuck Norris, but being an ass-hole to the team. That's a fail.

### Drop out

For crew that leave the project after they eligible for profit sharing, the owners and said crew will need to discuss about the outcome. If said crew contributed a significant amount to project's progress, we shall retain the accumulated time the crew spent with us, and share the profit with said crew.

But if the reason of leaving is because a lack of performance even after a few chances to fix the job performance issue.  The company reserve the rights to discount or void the accumulated time with the team for profit sharing.

## Equity share

We have no plan to sell, raise fund or public listing the company. Inviting big money comes big appetite for profit, which may steer us off-course from how we really want it to be. So giving out equity shares are meaningless. What's there for others if they can never sell their share?

If we ever going to sell the company, or list it, we will cut out a percentage of that deal and distribute among the team. How much? Don't know.

---

## End

Big part of this plan is just big dream now. We are still building our first product to show interested buyer. Some might think I am over thinking or over planning at this point.

My take is: This plan is fair and simple enough that we don't need wait for any real monetary figure to justify does it make sense. I think this plan is fair, and have no intention to change it for a long time. If for some reason a potential new joiner is not happy with it, then we cut short the collaboration and skip all the stress to negotiate when money comes in.

I am always looking for more! Curious? Talk to me!

---
title: "Hiring A Programmer"
date: 2019-08-09T10:20:09+08:00
draft: false
summary: ANA is hiring a programmer
---

ANA is hiring a programmer to join our current team of 4 (product designer, UX/UI designer+developer and 2 other programmers).

## About ANA

We are a small start up team building a Reliability Engineering web application.

One of ANA's co-founder is a veteran in this field. Haunted by bad software which fails to solve the real problem, he decided to work out a better solution. His 10 years of hands-on experience and a passion to improve working conditions in this field forms the very foundation and vision of ANA.

ANA does not go around town and raise funds from investors. We are old school. We see a market need not being fulfilled, we fulfill that need - serving the underserved. We will grow slowly by generating income through selling a precise solution to this market.

Bad news is? We are still in the very early stages of building a minimum viable product (MVP) which also serve as our sales pitch to potential customers. We cannot pay anyone monthly compensation so if you need a paycheck, that is not something we can provide now. But if you are able to sustain for a while and interested to be part of the pioneer team to push ANA out the door then read on! (More information about compensation will come later).

## How We Work

We are tiny and in the early stages of a startup, but rest assured, we are pretty structured in terms of solving problems as a team. Recently, the whole team jumped onto the bandwagon of [ShapeUp](https://basecamp.com/shapeup) and put it into practice diligently.

It fits us right since we are ramping up feature after feature for our upcoming MVP. If you join us, the team will become 5. And as a single unit, we will shape up a feature, work on it in a 3-4 weeks cycle, test and ship it before we move onto the next feature.

There isn’t a specific domain of problem that needs attention e.g. frontend, backend, ops, etc, so this job will throw different, unexpected and exotic problems at you. It is totally fine if you have never solved some of these problems before. Taking the steps to learn, trial and error and eventually solving the problem is what our team is all about.

## About the Job

The job requires you to develop [web application in Dart](https://angulardart.dev/). We do not have a server running in some remote hardware. We use Dart to write "backend" code and communicate directly with the database, [more information here](https://yuan-kuan.gitlab.io/post/angulardart-couchdb-fullstack/). It is Dart through and through. **We do not require you to know Dart beforehand**. Just be ready to learn it. It is easy to pick up! In fact, ANA's tech co-founder just started learning it a year ago. Which means if you start now, he’s only 1 year ahead of you.

Here are some real tasks our programmers worked on for the past few weeks:

- Wire up a bunch of HTML Angular template to frontend code: give life to the buttons, and populate the beautiful list with dynamic data.
- Write unit test for our PouchDB adapter (in Dart).
- Improve a Shell script, which helps cloning remote CouchDB into local machine.
- Implement a git-like system for enterprise asset management software, which is ANA. Still an on-going big project.
- Dive deep into [AngularDart Material Component](https://dart-lang.github.io/angular_components/), learn how to hack them and fit to our needs.
- Boot up a new feature with minimum UI, experimenting with hardest problem first. Provide proof of concept and pass on the branch for designer to beautify it.

This is not the full picture but only a snapshot of the work we've done in the previous month or 2. If you found this interesting or think that we are missing some really important works that you can help, please apply for this job!

## About You

You have to **love solving problems** and not afraid to be beaten down by it. The team is here to help you, or we take the beating together before rising again. We might not have a lot to teach you off the shelf but we'll give you all the room you’ll need to learn how to deal with problems. Self taught, independent and having the strength to ask for help when you get stuck for too long are the values we look up to.

You don't need any qualification for this. If you’ve read this far, you understand English, which is the most important thing because we need a good communicator. Of course you have to be good in programming regardless of the language or tools. Dart is new for almost everyone but having the courage and interest to learn a new language and skill, at any stage of your programming career, is again, another value we respect.

No one at ANA has the appetite to spoon feed or micro-manage anyone. This is not a place that has a time-framed task list handed to you where you just type away, ticking boxes to boxes. Everyone will contribute to the product directly, whether it's design or implementation or quality. Expressing your point of view is important and we sort of expect it too! Ask for forgiveness, not permission.

This is a remote work. No one is tracking the hours you have worked for ANA. Just make sure it is less than 8 hours (if you are full time). It comes with a great deal of freedom and we trust you to harness it for the best of your productivity and well-being. This includes having enough time to take care of your life outside of work, resting well and not getting stressed out by working.

## Compensation

Mentioned earlier: We can't pay a monthly salary at the moment. If you join now, profit sharing for your part will be calculated. We have a very [elaborated compensation plan](https://yuan-kuan.gitlab.io/post/fair-compensation/). Please have a look. Be warned, it isn’t a short read but perfect for those who enjoy the nitty gritty details.

## How to Apply

Just drop an email to dev@ana.industries. We are particularly interested in your recent working experience, problem you have solved or been solving, and the motive of applying for the job. Projects or code snippets online is a plus. Resume is another plus. Talk to us!

## Interview Process

If we communicated well with each other, the second step will be setting up a short 1-2 weeks (depends on your availability) open source project collaboration. We will work on a project together and find out each other’s coding style, skill and chemistry. From there on, we will find out whether ANA is a team you’d like to work with and whether you are a right fit for the team.
